
class Stats {
public:
	float min, max, mean, stddev;
	int non_zero;
	Stats(float *data, int length);
};