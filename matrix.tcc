#ifndef MATRIX_TCC
#define MATRIX_TCC

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <fftw3.h>
#include "matrix.h"

using namespace std;


extern "C" {
  void do_fft(double *image, int nx, int ny, fftw_complex *output);
  void do_ifft(fftw_complex *fft_in, int nx, int ny, double *output);
  void *memset(void *s, int c, size_t n);
}

template<typename T>
Matrix<T>::Matrix() {
  //cout << "Construct empty\n";
  data = NULL;
  rows = cols =  0;
  is_fft = false; 
}

template<typename T>
Matrix<T>::Matrix(int r, int c) {
  //cout << "Construct from row and column spec\n";

  if ( (data=new T*[r]) == NULL ) {
    cerr << "Failed to get memory for data in Matrix\n";
    exit(1);
  }

  for (int i=0; i<r; ++i) {
    if ( (data[i]=new T[c]) == NULL ) {
      cerr << "Failed to get memory for data[i] in Matrix\n";
      exit(1);
    }
    memset(data[i], 0, c*sizeof(T));
  }
	
  rows = r; cols = c; is_fft = false;

}

// Constructor based on existing Matrix
template<typename T>
Matrix<T>::Matrix(const Matrix& m) {
  int mr, mc;

  //cout << "Construct from another matrix\n";

  m.dimensions(mr,mc);

  // Insert new
  if ( (data=new T*[mr]) == NULL ) {
    cerr << "Failed to get memory for data in Matrix constructor\n";
    exit(1);
  }
  for (int i=0; i<mr; ++i)  
    if ( (data[i]=new T[mc]) == NULL ){
    cerr << "Failed to get memory for data[i] in Matrix constructor\n";
    exit(1);
  }  
  
  rows = mr; cols = mc; is_fft = m.is_fft;

  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) data[i][j] = m.get(i, j);
}

template<typename T>
void Matrix<T>::vertical_flip() {
  Matrix<T> tmp_matrix(rows, cols);
  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) tmp_matrix[i][j] = data[rows-i-1][j];
  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) data[i][j] = tmp_matrix[i][j];
} 

template<typename T>
Matrix<T>::~Matrix() {
  //cout << "Destruct\n";

  if ( data == NULL ) return;

  for (int i=0; i<rows; ++i)
    if ( data[i] != NULL ) {  delete[] data[i]; data[i] = NULL; }
  delete[] data; data = NULL;
  rows = cols = 0;
}

 
// numpy fftshift. Same as ifftshift if the array dimension is even
template<typename T>
void Matrix<T>::fftshift() {
  Matrix<T> tmp(rows, cols);
  int u, v, x, y;

  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) {
      u = i-rows/2;
      v = j-cols/2;
      if ( 0 <= u ) x = u;
      else x = u+rows;
      if ( 0 <= v ) y = v;
      else y = v+cols;
      tmp[x][y] = data[i][j];
    }

  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) data[i][j] = tmp[i][j];

}


template<typename T>   
Matrix<T> Matrix<T>::transpose() {  
  Matrix<T> ct(rows, cols);
 
  for (int i=0; i<rows; ++i)
    for (int j=i+1; j<cols; j++ ) {
      ct[i][j] = data[i][j];
  }
  return ct;
}

template<typename T>
T *Matrix<T>::operator[](int r) {                  // index from 0, 0
  if ( r < 0 || r >= rows ) {
    cerr << "Bad index value to access Matrix\n";
    exit(1);
  }
  return data[r]; 
}         
  
template<typename T>
void Matrix<T>::make_positive() {                  // index from 0, 0
  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j)
      if ( data[i][j] < 0 ) data[i][j] = 0;
}         

template<typename T>
T Matrix<T>::max() {                  // index from 0, 0
  T max_val = data[0][0];
  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j)
      if ( abs(data[i][j]) > max_val ) max_val = data[i][j];
}         


  


// Displayed with rows going down 
template<typename T>
void Matrix<T>::print() {
  for (int i=0; i<rows; ++i) {
    for (int j=0; j<cols; ++j)
      cout << data[i][j] << " ";
    cout << endl;
  } 
}

template<typename T>
Matrix<T> Matrix<T>::operator*(const Matrix& m1) {
  int mr, mc;

  m1.dimensions(mr,mc);

  Matrix newm(rows,mc);

  for (int i=0; i<rows; ++i)
    for (int j=0; j<mc; ++j) {
      newm[i][j] = 0.0;
      for (int k=0; k<cols; ++k) {   
	newm[i][j] += data[i][k]*m1[k][j]; //m1[k][j];
      }
    }

  return newm;
}

template<typename T>
Matrix<T> Matrix<T>::operator-(const Matrix& m1) {
  Matrix newm(rows,cols);
	
  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j) 
      newm[i][j] = data[i][j]-m1.get(i,j);
		
  return newm;
}  

template<typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix& m1)  {
  int mr, mc;

  //cout << "Destruct and Construct for assignment\n";

  m1.dimensions(mr,mc);

  // Destroy current
  if ( data != NULL ) {
    for (int i=0; i<rows; ++i)
      if ( data[i] != NULL ) { delete[] data[i]; }
    delete[] data; 
  }

  // Insert new
  if ( (data=new T*[mr]) == NULL ) {
    cerr << "Failed to get memory for data in Matrix assignment\n";
    exit(1);
  }
  for (int i=0; i<mr; ++i) 
    if ( (data[i]=new T[mc]) == NULL ) {
      cerr << "Failed to get memory for data[i] in Matrix assignment\n";
      exit(1);
  
    }

  rows = mr; cols = mc; is_fft = m1.is_fft;

  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) data[i][j] = m1[i][j];

  return *this;
}

template<typename T>
void Matrix<T>::cube() {
  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j) data[i][j] = data[i][j]*data[i][j]*data[i][j];
}

template<typename T>
Matrix<T>::Matrix(const char *file_name) {
  ifstream file(file_name);
  int dimension, i, j;
  string line;

  //cout << "Construct from map in a file\n";

  if ( file.is_open() ) {
    getline(file, line);
    dimension = atoi(line.c_str());
    data = new T*[dimension];
    for (i=0; i<dimension; ++i) data[i] = new T[dimension];

    i = j = 0;
    while( getline(file, line) ) {
      istringstream istr(line);
      istr >> data[i][j];
      ++j; if ( j >= dimension ) { j = 0; ++i; }
    }

    rows = cols = dimension;
  } else {
      cerr << "Failed to open map file: " << file_name << endl;
      exit(1);
  
  }
  is_fft = false;
}



template<typename T>
void Matrix<T>::save_map(const char *file_name) {
  ofstream file(file_name);
  int dimension, x, y;
  string line;

  if ( rows != cols ) {
    cout << "Warning: expanding non-square matrix for saving\n";
    Matrix<T> fill_out((rows>cols?rows:cols), (rows>cols?rows:cols));
    for (int i=0; i<rows; ++i) for (int j=0; j<cols; ++j) fill_out[i][j] = data[i][j];
    fill_out.save_map(file_name);
    return;
  }

 
  if ( file.is_open() ) {
    file << rows << endl;
    for (int i=0; i<rows; ++i) for (int j=0; j<cols; ++j)
      file << data[i][j] << endl; 
    file.close();
  } else {
      cerr << "Failed to save to map file: " << file_name << endl;
      exit(1);
  }
}

template<typename T>
Matrix< complex<float> > Matrix<T>::fft() {
  double *image;
  int cols_fft;
  fftw_complex *out_fft;

  if ( is_fft ) {
    cerr << "Attempt to fft a Matrix that already an FFT\n";
    exit(1);
  }

  if ( !( rows == cols && rows%2 == 0 && cols%2 == 0 ) ) {
    cerr << "Attempt to fft a Matrix that is not square and/or has a dimension not a power of 2\n"; 
    exit(1);
  }

  // Need to flatten the data
  image = new double[rows*cols];
  for (int i=0; i<rows; ++i) for (int j=0; j<cols; ++j)
    image[i*cols+j] = data[i][j];

  // Find out the space for the FFT
  cols_fft = (cols/2)+1;

  out_fft = new fftw_complex[rows*cols_fft];

  do_fft(image, rows, cols, out_fft);

  Matrix< complex<float> > fft_Matrix(rows, cols_fft);

  for (int i=0; i<rows; ++i) for (int j=0; j<cols_fft; ++j) 
    fft_Matrix[i][j] = complex<float>(out_fft[i*cols_fft+j][0], out_fft[i*cols_fft+j][1]);

  fft_Matrix.is_fft = true;

  return fft_Matrix;

}


  

#endif
