// gcc levmar.c -Llevmar-2.6 -Ilevmar-2.6 -llevmar -llapack -lblas -lgfortran -lm
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <levmar.h>


#define SQR(x) ((x)*(x))

// Some things are kept in memory for access by the fit function
static double **original_gauss;
static double **estimated_gauss;


static double **make_gauss(double semimaj, double semimin, double theta, int x0, int y0, double height, int naxis) {
  double a, b, c;
  double **gauss;

  int i, j;
  
  
  if ( (gauss=(double**)malloc(naxis*sizeof(double*))) == NULL ) {
    fprintf(stderr,"Failed to allocate memory for gauss fitting\n");
    exit(1);
  }
  for (i=0; i<naxis; ++i)
    if ( (gauss[i]=(double*)malloc(naxis*sizeof(double))) == NULL ) {
    fprintf(stderr,"Failed to allocate memory for gauss fitting\n");
    exit(1);
  }
 
  a = SQR(cos(theta)) / 2 / SQR(semimaj)
                  + SQR(sin(theta)) / 2 / SQR(semimin); /// http://en.wikipedia.org/wiki/Gaussian_function
  b = -sin(2 * theta) / 4 / SQR(semimaj)
                  + sin(2 * theta) / 4 / SQR(semimin);
  c = SQR(sin(theta)) / 2 / SQR(semimaj)
                  + SQR(cos(theta)) / 2 / SQR(semimin);
		  
  for (i = 0; i<naxis; ++i)
    for (j = 0; j<naxis; ++j) {
       gauss[i][j] = height*exp(-(a * SQR(i-naxis/2-x0)+ 2 * b * (i-naxis/2 - x0)* (j-naxis/2-y0)+ c * SQR(j-naxis/2-y0)));
       
    }
  return gauss;
}


static void errorfunc(double *p, double *hx, int m, int n, void *adata) {
	// make a gaussian and compare it
  double a, b, c, semimaj, semimin, theta, height;
  int i, j, k;
  double x0, y0;
  unsigned int naxis = *((unsigned int*)adata);
 
  semimaj = p[0]; semimin = p[1]; theta = p[2]; height = p[3];
  x0 = p[4]; y0 = p[5];

  a = SQR(cos(theta)) / 2 / SQR(semimaj)
                  + SQR(sin(theta)) / 2 / SQR(semimin); /// http://en.wikipedia.org/wiki/Gaussian_function
  b = -sin(2 * theta) / 4 / SQR(semimaj)
                  + sin(2 * theta) / 4 / SQR(semimin);
  c = SQR(sin(theta)) / 2 / SQR(semimaj)
                  + SQR(cos(theta)) / 2 / SQR(semimin);
      
  for (i = 0; i<naxis; ++i)
    for (j = 0; j<naxis; ++j) {
       estimated_gauss[i][j] = height*exp(-(a * SQR(i-naxis/2-x0)+ 2 * b * (i-naxis/2 - x0)* (j-naxis/2-y0)+ c * SQR(j-naxis/2-y0)));
       
    }
  k = 0;
  for (i = 0; i<naxis; ++i)
    for (j = 0; j<naxis; ++j) { 
	hx[k] = fabs(estimated_gauss[i][j]-original_gauss[i][j]);
        ++k;
    }

} 

int levmar_fit(double *in_data, unsigned int pixel_width, double *params, unsigned int num_params) {
  int i, j, index;
  //for (i=0; i<10; ++i) printf("%f\n",in_data[i]);
  if ( (original_gauss=(double**)malloc(pixel_width*sizeof(double*))) == NULL ) {
    fprintf(stderr,"Failed to allocate memory for gauss fitting\n");
    exit(1);
  }
  for (i=0; i<pixel_width; ++i)
    if ( (original_gauss[i]=(double*)malloc(pixel_width*sizeof(double))) == NULL ) {
      fprintf(stderr,"Failed to allocate memory for gauss fitting\n");
      exit(1);
  }
  index = 0;
  for (i=0; i<pixel_width; ++i) for (j=0; j<pixel_width; ++j) original_gauss[i][j] = in_data[index++];
  if ( (estimated_gauss=(double**)malloc(pixel_width*sizeof(double*))) == NULL ) {
    fprintf(stderr,"Failed to allocate memory for gauss fitting\n");
    exit(1);
  }
  for (i=0; i<pixel_width; ++i)
    if ( (estimated_gauss[i]=(double*)malloc(pixel_width*sizeof(double))) == NULL ) {
      fprintf(stderr,"Failed to allocate memory for gauss fitting\n");
      exit(1);
  }
	  
  return dlevmar_dif(errorfunc, params, NULL, num_params, pixel_width*pixel_width, 100, NULL, NULL, NULL, NULL, &pixel_width);
}


#define NUM_PARAMS 6
#define NAXIS 11
void main() {
	double **test_data, flat[NAXIS*NAXIS];
	double a, b, c;
	double semimaj = 2;
	double semimin = 1;
	double height = 100;
	double theta = 30/180.0*3.14159;
	double params[NUM_PARAMS];
	int i, j, index;


	printf("semimaj,semimin,theta,height in %f %f %f %f\n",semimaj,semimin,theta,height);
 	
	test_data = make_gauss(semimaj, semimin, theta, 0, 0, height, NAXIS);
	index = 0;
        for (i=0; i<NAXIS; ++i) for (j=0; j<NAXIS; ++j) flat[index++] = test_data[i][j];

	params[0] = semimaj*1.2; params[1] = semimin*0.8; params[2] = theta*1.1; params[3] = height*0.96;
	params[4] = 0.1; params[5] = 0.1;


	printf("%d\n",levmar_fit(flat,NAXIS,params,NUM_PARAMS));

	printf("semimaj,semimin,theta,height out %f %f %f %f\n",params[0],params[1],params[2],params[3]);
}


