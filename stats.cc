#include <cmath>
#include "stats.h"

#define SQR(x) ((x) * (x))

Stats::Stats(float *data, int length) {
	float sum=0;
	int num_values=0;
	
	min = 1e39;
	max = -1e39;
	non_zero = 0;
	sum = 0;

	for (int i=0; i <length; ++i) {
		if ( data[i] < min ) min = data[i];
		if ( data[i] > max ) max = data[i];
		if ( data[i] != 0.0 ) ++non_zero;
		if ( data[i] != 0.0 ) { sum += data[i]; ++num_values; }
	}
	mean = sum/num_values;
	stddev = 0;
	for (int i=0; i<length; ++i) if ( data[i] != 0 ) stddev += SQR(data[i]-mean);
	stddev = sqrt(stddev/num_values);

}