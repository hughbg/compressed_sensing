#include <complex>
#include "matrix.tcc"

template<>
class matrix< complex<float> > {
private:
  complex<float>  **data;
  int rows, cols;
  char *name;


public:
  bool is_fft;

  matrix(int, int);
  matrix();
  matrix(const matrix&);     // very important to have a constructor of this form
                        	// so that objects returned from a function on the stack are
                        	// copied properly. Also need a constructor for empty.
  matrix(const char *file_name);
  ~matrix();
  complex<float> & val(int i,int j) const;    // index from 1, 1
  complex<float>  *operator[](int r);        // index from 0, 0
  const matrix< complex<float> > operator-(const matrix< complex<float> >&) const;
  const matrix operator*(const matrix&) const;
  const matrix operator*(const float val) const;
  const matrix& operator=(const matrix&) const;
  void conjugate_transpose();
  void apply_mask(const matrix<bool>& mask);
  void fill(complex<float> c);
  int ncols() const { return cols; }
  int nrows() const { return rows; }
  matrix< complex<float> > fft();
  matrix<double> ifft();
Matrix< complex<float> > Matrix< complex<float> >::make_as_fft() {
  void dimensions(int& r, int& c) const { r = rows; c = cols; }
  void save_map(const char *file_name);
  void print();
};    
