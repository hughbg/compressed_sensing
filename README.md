# README #

Contains code for implementing compressed sensing in the imaging pipeline of the OVRO-LWA Telescope.

It is built on wavelet code obtained from SASIR (http://www.cosmostat.org/software/sasir) but other aspects are resdesigned for implementation in software specific to OVRA-LWA and LEDA (http://www.tauceti.caltech.edu/leda/).

See run\_cs.cc for a test driver and usage.