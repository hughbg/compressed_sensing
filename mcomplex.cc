#include <cstdlib>
#include <iostream>
#include <fftw3.h>
#include "matrix.tcc"

extern "C" {
  void *memset(void *s, int c, size_t n);
  void do_fft(double *image, int nx, int ny, fftw_complex *output);
  void do_ifft(fftw_complex *fft_in, int nx, int ny, double *output);
}


// Specialization of template for complex array. These are now no longer a template so have
// to be treated as a .o file.

using namespace std;

Matrix< complex<float> >::Matrix(const Matrix< complex<float> >& m) { 
  int mr, mc;

  //cout << "Construct from another matrix\n";

  m.dimensions(mr,mc);

  // Insert new
  if ( (data=new complex<float>*[mr]) == NULL ) {
    cerr << "Failed to get memory for data in Matrix constructor\n";
    exit(1);
  }
  for (int i=0; i<mr; ++i)  
    if ( (data[i]=new complex<float>[mc]) == NULL ){
    cerr << "Failed to get memory for data[i] in Matrix constructor\n";
    exit(1);
  }  
  
  rows = mr; cols = mc; is_fft = m.is_fft;

  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) data[i][j] = m.get(i, j);
}


Matrix< complex<float> >::Matrix(const char *file_name) {
  ifstream file(file_name);
  int dimension, i, j;
  string line;

  //cout << "Construct from map in a file\n";

  if ( file.is_open() ) {
    getline(file, line);
    dimension = atoi(line.c_str());
    data = new complex<float>*[dimension];
    for (i=0; i<dimension; ++i) data[i] = new complex<float>[dimension];

    i = j = 0;
    while( getline(file, line) ) {
      istringstream istr(line);
      istr >> data[i][j]; 
      ++j; if ( j >= dimension ) { j = 0; ++i; }
    }

    rows = cols = dimension;
  } else {
      cerr << "Failed to open map file: " << file_name << endl;
      exit(1);
  
  }
  is_fft = false;

}


Matrix<float> Matrix< complex<float> >::ifft() {
  int cols_image;
  double *image_out;
  fftw_complex *fft;

  if ( !is_fft ) {
    cerr << "Attempt to ifft a Matrix that is not an FFT\n";
    exit(1);
  }

  // Need to flatten the data
  fft = new fftw_complex[rows*cols];
  for (int i=0; i<rows; ++i) for (int j=0; j<cols; ++j) {
    fft[i*cols+j][0] = data[i][j].real();
    fft[i*cols+j][1] = data[i][j].imag();
  }

  cols_image = (cols-1)*2;
  image_out = new double[rows*cols_image];

  do_ifft(fft, rows, cols_image, image_out);

  Matrix<float> image(rows, cols_image);

  for (int i=0; i<rows; ++i) for (int j=0; j<cols_image; ++j) 
    image[i][j] = image_out[i*cols_image+j]/rows/cols_image;

  return image;
}

// numpy fftshift. Same as ifftshift if the array dimension is even
// Only works on square matrices.
void Matrix<complex<float> >::fftshift() {
  if ( rows != cols ) {
    cerr << "Attempt to fftshift a matrix that is not square\n"; 
    exit(1);
  }

  Matrix<complex<float> > tmp(rows, cols);
  int u, v, x, y;

  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) {
      u = i-rows/2;
      v = j-cols/2;
      if ( 0 <= u ) x = u;
      else x = u+rows;
      if ( 0 <= v ) y = v;
      else y = v+cols;
      tmp[x][y] = data[i][j]; 
    }

  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) data[i][j] = tmp[i][j];

}

  
void Matrix< complex<float> >::chop_negative() {
  if ( !( rows == cols && rows%2 == 0 && cols%2 == 0 ) ) {
    cerr << "Attempt make an fft that is not square and/or has a dimension not a power of 2\n"; 
    exit(1);
  }


    // Find out the space for the FFT
  int cols_fft = (cols/2)+1;

  for (int i=0; i<rows; ++i) {
    complex<float> *new_row = new complex<float>[cols_fft];
    for (int j=0; j<cols_fft; ++j) new_row[j] = data[i][j];
    delete[] data[i];
    data[i] = new_row;
  }    
  cols = cols_fft;
  is_fft = true;

}

void Matrix< complex<float> >::flip() {
  if ( rows != cols ) {
    cerr << "Attempt to flip a matrix that is not square\n"; 
    exit(1);
  }
  for (int i=0; i<rows/2; ++i)
    for (int j=0; j<cols; ++j) {
       complex<float> tmp = data[i][j];
       data[i][j] = data[rows-i-1][cols-j-1];
       data[rows-i-1][cols-j-1] = tmp;
    }
}


Matrix< complex<float> > Matrix< complex<float> >::conjugate_transpose() {
  Matrix< complex<float> > ct(rows, cols);    // goes in here
   
  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; j++ ) 
      ct[i][j] = conj(data[j][i]);
  

  return ct;
}


void Matrix< complex<float> >::apply_mask(const Matrix<bool>& mask) {
  if ( mask.nrows() != rows || mask.ncols() != cols ) {
    cerr <<"Error: Mask size is not the same as FFT size\n";
    exit(1);
  } 
  int sum=0;
  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j)
      if ( !mask.get(i, j) ) ++sum;
  //cout << 100*(rows*cols-(float)sum)/rows/cols << "% unmasked\n";

  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j)
      if ( !mask.get(i, j) ) data[i][j] = complex<float>(0, 0);
}

void Matrix< complex<float> >::fill(complex<float> c) {
  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j)
      data[i][j] = c;
}


Matrix< complex<float> >::~Matrix() {
  //cout << "Destruct\n";

  if ( data == NULL ) return;

  for (int i=0; i<rows; ++i)
    if ( data[i] != NULL ) {  delete[] data[i]; data[i] = NULL; }
  delete[] data; data = NULL;
  rows = cols = 0;
}


Matrix< complex<float> >::Matrix(int r, int c) {
  //cout << "Construct from row, column spec\n";

  if ( (data=new complex<float>*[r]) == NULL ) {
    cerr << "Failed to get memory for data in Matrix\n";
    exit(1);
  }

  for (int i=0; i<r; ++i) {
    if ( (data[i]=new complex<float>[c]) == NULL ) {
      cerr << "Failed to get memory for data[i] in Matrix\n";
      exit(1);
    }
    memset(data[i], 0, c*sizeof(complex<float>));
  }

	
  rows = r; cols = c;
  is_fft = false;
}


void Matrix< complex<float> >::print() {
  for (int i=0; i<rows; ++i) {
    for (int j=0; j<cols; ++j)
      cout << data[i][j] << " ";
    cout << endl;
  } 
}


Matrix< complex<float> > Matrix< complex<float> >::operator-(const Matrix< complex<float> >& m1) {
  if ( rows != m1.nrows() || cols != m1.ncols() ) {
    cerr << "Attempt to subtract complex arrays of different sizes\n";
    exit(1);
  }

  Matrix< complex<float> > newm(rows,cols);
	
  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j) {
      newm[i][j] = data[i][j]-m1.get(i, j);

    }

  newm.is_fft = ( is_fft && m1.is_fft );
		
  return newm;
}  

Matrix< complex<float> >::operator JonesMatrix() {
  JonesMatrix jm;

  for (int i=0; i<rows; ++i) 
    for (int j=0; j<cols; ++j) {
      jm[i][j] = data[i][j];
    }
  return jm;
}


complex<float> *Matrix< complex<float> >::operator[](int r) {                  // index from 0, 0
  if ( r < 0 || r >= rows ) {
    cerr << "Bad index value to access Matrix\n";
    exit(1);
  }
  return data[r]; 
}   


Matrix<complex<float> > Matrix< complex<float> >::operator*(const Matrix< complex<float> >& m1) {
  int mr, mc;

  m1.dimensions(mr,mc);

  Matrix< complex<float> > newm(rows,mc);

  for (int i=0; i<rows; ++i)
    for (int j=0; j<mc; ++j) {
      newm[i][j] = 0.0;
      for (int k=0; k<cols; ++k) {   
	newm[i][j] += data[i][k]*m1.get(k, j); //m1[k][j];
      }
    }

  return newm;
}

Matrix<complex<float> > Matrix< complex<float> >::operator*(const float val) {
  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) {
      data[i][j] *= val;
    }

  return *this;
}

Matrix<complex<float> >& Matrix< complex<float> >::operator+=(const Matrix< complex<float> >& m1) {
  int mr, mc;

  m1.dimensions(mr,mc);


  for (int i=0; i<rows; ++i)
    for (int j=0; j<mc; ++j) {
      data[i][j] += m1.get(i, j);
    }

  return *this;
}


Matrix< complex<float> >& Matrix< complex<float> >::operator=(const Matrix< complex<float> >& m1)  {
  int mr, mc;

  //cout << "Destruct and construct for assignment\n";

  m1.dimensions(mr,mc);

  // Destroy current
  if ( data != NULL ) {
    for (int i=0; i<rows; ++i)
      if ( data[i] != NULL ) { delete[] data[i]; }
    delete[] data; 
  }

  // Insert new
  if ( (data=new complex<float>*[mr]) == NULL ) {
    cerr << "Failed to get memory for data in Matrix assignment\n";
    exit(1);
  }
  for (int i=0; i<mr; ++i) 
    if ( (data[i]=new complex<float>[mc]) == NULL ) {
      cerr << "Failed to get memory for data[i] in Matrix assignment\n";
      exit(1);
  
    }

  rows = mr; cols = mc; is_fft = m1.is_fft;

  for (int i=0; i<rows; ++i)
    for (int j=0; j<cols; ++j) data[i][j] = m1.get(i, j);

  return *this;
}


void Matrix<complex<float> >::save_map(const char *file_name, bool abs_only) {
  ofstream file(file_name);
  int dimension, x, y;
  string line;

  if ( rows != cols ) {
    cout << "Warning: expanding non-square matrix for saving\n";
    Matrix<complex<float> > fill_out((rows>cols?rows:cols), (rows>cols?rows:cols));
    for (int i=0; i<rows; ++i) for (int j=0; j<cols; ++j) fill_out[i][j] = data[i][j];
    fill_out.save_map(file_name, abs_only);
    return;
  }

 
  if ( file.is_open() ) {
    file << rows << endl;
    for (int i=0; i<rows; ++i) for (int j=0; j<cols; ++j)
      if ( abs_only ) file << abs(data[i][j]) << endl; 
      else file << data[i][j] << endl; 
    file.close();
  } else {
      cerr << "Failed to save to map file: " << file_name << endl;
      exit(1);
  }
}


