
#include <vector>
using namespace std;

#define LEN 100

class Histogram {
	float histogram[LEN];
	float hist_min, hist_max;
	
public:
	Histogram() { for (int i=0; i<LEN; ++i) histogram[i] = 0; hist_min = hist_max = 0; }
    float percentile(float p, float *data, int length);
	void make_histogram(float *data, int length);
	void print();
};