#include "wavelets.h"
#include "stats.h"
#include "matrix.tcc"

#define SQR(x) ((x)*(x))

static const int NWSCALE = 7;

Wavelets::Wavelets(Matrix<float>& m) {
  tk = told = 1;

  image.alloc(m.nrows(), m.ncols(), 0);
  wavelets.alloc(image, NWSCALE);
  wavelets.AdjointRec = True;

  thresholded_coefficients = new float[wavelets.size_transform()];
  previous_thresholded_coefficients = new float[wavelets.size_transform()];
  working_coefficients = new float[wavelets.size_transform()];	// these are used for reconstruction, and to receive the increment for the gradient step

  for (int i=0; i<m.nrows(); ++i)
    for (int j=0; j<m.ncols(); ++j) 
      image(i, j) = m[i][j];
  wavelets.transform(image, working_coefficients);
  memcpy(thresholded_coefficients, working_coefficients, length_coefficients()*sizeof(float));

}

Wavelets::~Wavelets() {
  delete[] previous_thresholded_coefficients; delete[] thresholded_coefficients; delete[] working_coefficients;
}

Matrix<float> Wavelets::reconstruct(int level) {
  if ( level != ALL_LEVELS ) {
	  int num_values = length_coefficients()/NWSCALE;
	  for (int i=0; i<length_coefficients(); ++i) 
		  if ( i < level*num_values || i >= (level+1)*num_values ) working_coefficients[i] = 0;
  }
	
  wavelets.recons(working_coefficients, image);

  Matrix<float> m(image.nx(), image.ny());

  for (int i=0; i<m.nrows(); ++i)
    for (int j=0; j<m.ncols(); ++j) 
      m[i][j] = image(i, j);

  return m;
}

void Wavelets::add_coefficients(Wavelets& new_w, float mu) {
  if ( wavelets.size_transform() != new_w.size_transform() ) {
    cout << "Error: attempt to add wavelets of different dimensions\n";
    exit(1);
  }

  for (int i=0; i<length_coefficients(); ++i) working_coefficients[i] += new_w.coefficient(i)*mu;
}


void Wavelets::soft_threshold(float lvl, bool threshold_coarse) {
	  int last;
	  int num_values = length_coefficients()/NWSCALE;
	  
	  if ( threshold_coarse ) last = length_coefficients();
	  else last = length_coefficients()-num_values;

	  memcpy(previous_thresholded_coefficients, thresholded_coefficients, length_coefficients()*sizeof(float));

	  for (int i=0; i<last; i++)
		  working_coefficients[i] = ::soft_threshold(working_coefficients[i], lvl);
	  
	  memcpy(thresholded_coefficients, working_coefficients, length_coefficients()*sizeof(float));
}


void Wavelets::stats() {
  int num_values = length_coefficients()/NWSCALE;
  
  for (int level=0; level<NWSCALE; ++level) {
 	
	Stats stats(&working_coefficients[level*num_values], num_values);

    cout << "Wavelets level " << level << " Min: " << stats.min << " Max: " << stats.max << " Mean: " << stats.mean << " Std dev: " << stats.stddev << " Fill factor: " << (float(stats.non_zero)/num_values)*100 << "% (" << stats.non_zero << ")" << endl; 
  }
}

  

void Wavelets::fast() {
  told = tk;
  tk = (1+sqrt(1+4*told*told))/2;
  for(int k=0; k<length_coefficients(); k++) 
    working_coefficients[k] = thresholded_coefficients[k] + (told-1)/tk * (thresholded_coefficients[k]-previous_thresholded_coefficients[k]); 
}         

