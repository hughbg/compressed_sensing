#include "SB_Filter_float.h"
#include "matrix.h"

#define ALL_LEVELS -1

class Wavelets {
  ATROUS_2D_WT_float wavelets; 
  fltarray image;
  float *thresholded_coefficients, *previous_thresholded_coefficients, *working_coefficients;
  float told, tk;

public:
  Wavelets(Matrix<float>&);
  ~Wavelets();

  Matrix<float> reconstruct(int level=ALL_LEVELS);
  void soft_threshold(float lvl, bool threshold_coarse=false);
  void add_coefficients(Wavelets& new_w, float mu);
  void fast();
  int length_coefficients() { return wavelets.size_transform(); }  // Should be nx*ny*scale
  float coefficient(int i) { return working_coefficients[i]; }
  int size_transform() { return wavelets.size_transform(); }
  void stats();
};

