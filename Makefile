run_cs: run_cs.cc matrix.tcc matrix.h fftw.o wavelets.cc wavelets.h mcomplex.cc histogram.h histogram.cc stats.h stats.cc
	g++ -o run_cs run_cs.cc wavelets.cc mcomplex.cc fftw.o histogram.cc stats.cc -lfftw3 -L. -lwavelets    # get libwavelets from UV_Inpainting

gaincal: gaincal.cc  matrix.tcc matrix.h fftw.o mcomplex.cc jones_matrix.cc
	g++ -o gaincal gaincal.cc jones_matrix.cc fftw.o mcomplex.cc -lfftw3 -Llevmar-2.6 -llevmar -llapack -lblas -lgfortran -lm

fftw.o: fftw.c
	gcc -c fftw.c
