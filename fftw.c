#include <fftw3.h>

// Keep the FFTW code away from cuda and nvcc

void do_fft(double *image, int nx, int ny, fftw_complex *output) {
  fftw_plan plan_forward;
 
  plan_forward = fftw_plan_dft_r2c_2d(nx, ny, image, output, FFTW_ESTIMATE );

  fftw_execute(plan_forward);
  fftw_free(plan_forward);
}


void do_ifft(fftw_complex *fft_in, int nx, int ny, double *output) {
  fftw_plan plan_backward;  

  plan_backward = fftw_plan_dft_c2r_2d (nx, ny, fft_in, output, FFTW_ESTIMATE );


  fftw_execute (plan_backward);
  fftw_free(plan_backward);
}

