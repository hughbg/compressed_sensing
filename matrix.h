#ifndef Matrix_H
#define Matrix_H

#ifndef FALSE
#define FALSE 0
#endif

#include <complex>
using namespace std;

template<typename T>
class Matrix {
private:
  T  **data;
  int rows, cols;
  char *name;
  bool is_fft;


public:
  Matrix(int, int);
  Matrix();
  Matrix(const Matrix&);     // copy constructor very important to have a constructor of this form
                        	// so that objects returned from a function on the stack are
                        	// copied properly. Also need a constructor for empty.
  Matrix(const char *file_name);
  ~Matrix();
  T *operator[](int r);        // index from 0, 0
  Matrix operator-(const Matrix&);
  Matrix operator*(const Matrix&);
  Matrix& operator=(const Matrix&);
  T max();
  void fftshift();
  void vertical_flip();
  void cube();
  Matrix transpose();
  T get(int r, int c) const { return data[r][c]; }
  void make_positive();
  int ncols() const { return cols; }
  int nrows() const { return rows; }
  Matrix< complex<float> > fft();
  void dimensions(int& r, int& c) const { r = rows; c = cols; }
  void save_map(const char *file_name);
  void print();
};    

class JonesMatrix;

template<>
class Matrix< complex<float> > {
private:
  int rows, cols;
  char *name;

protected:
  complex<float>  **data;


public:
  bool is_fft;

  Matrix(int, int);
  Matrix();
  Matrix(const Matrix< complex<float> >&);     // very important to have a constructor of this form
                        	// so that objects returned from a function on the stack are
                        	// copied properly. Also need a constructor for empty.
  Matrix(const char *file_name);
  ~Matrix();
  complex<float> *operator[](int r);        // index from 0, 0
  Matrix< complex<float> > operator-(const Matrix< complex<float> >&);
  Matrix< complex<float> > operator*(const float);
  Matrix< complex<float> > operator*(const Matrix< complex<float> >&);
  Matrix< complex<float> >& operator=(const Matrix< complex<float> >&);
  Matrix< complex<float> >& operator+=(const Matrix< complex<float> >&);
  operator JonesMatrix();	// type cast
  complex<float> get(int r, int c) const { return data[r][c]; }
  void set(int i, int j, complex<float> c) { data[i][j] = c; }
  Matrix< complex<float> > conjugate_transpose();
  void apply_mask(const Matrix<bool>& mask);
  void fill(complex<float> c);
  int ncols() const { return cols; }
  int nrows() const { return rows; }
  Matrix< complex<float> > fft();
  Matrix<float> ifft();
  void fftshift();
  void flip();
  void chop_negative();
  void dimensions(int& r, int& c) const { r = rows; c = cols; }
  void save_map(const char *file_name, bool abs_only=false);
  void print();
};    

class JonesMatrix : public Matrix< complex<float> > {
public:
  JonesMatrix() : Matrix< complex<float> >(2, 2) {}
  JonesMatrix(complex<float> a, complex<float> b, complex<float> c, complex<float> d) : Matrix< complex<float> >(2, 2) { set(a, b, c, d); }

  JonesMatrix inverse();
  void set(complex<float> a, complex<float> b, complex<float> c, complex<float> d) { data[0][0]=a; data[0][1]=b; data[1][0]=c; data[1][1]=d; }
  bool is_normal();
}; 

#endif 
