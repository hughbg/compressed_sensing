#include <iostream>
#include <cstdlib>
#include "histogram.h"

void Histogram::make_histogram(float *data, int length) {
  float min = 1e39;
  float max = -1e39;
  for (int i=0; i<length; ++i) {
	  if ( data[i] < min ) min = data[i];
	  if ( data[i] > max ) max = data[i];
  }
  
  for (int i=0; i<length; ++i) {
	  int bin = int((data[i]-min)/(max-min)*(LEN-1));
	  if ( bin >= LEN ) {
		  cerr << "Overflow in histogram, Bin: " << bin << endl;
		  exit(1);
	  }
	  histogram[bin]++;
  }
  
  hist_min = min; hist_max = max;
  
}

void Histogram::print() {
  for (int i=0; i<LEN; ++i) cout << hist_min+(i+0.5)*(hist_max-hist_min)/LEN << " " << histogram[i] << endl;   // 0.5 so in middle of bin
}

float Histogram::percentile(float p, float *data, int length) {
	float total_so_far = 0;
	
	make_histogram(data, length);
	for (int i=0; i<LEN; ++i) {
		total_so_far += histogram[i];
		if ( total_so_far >= length*p ) return hist_min+(i+0.5)*(hist_max-hist_min)/LEN;
	}
    return hist_min+((LEN-1)+0.5)*(hist_max-hist_min)/LEN;
}
/*
int main() {
	Histogram hist;
	//for (int i=0; i<1000; ++i) hist.accumulate(rand());
	hist.accumulate(1);
	hist.accumulate(1);
	hist.accumulate(1);
	hist.accumulate(2);
	hist.accumulate(2);
	hist.accumulate(3);
	hist.accumulate(3);
	hist.accumulate(3);
	hist.accumulate(3);
	hist.accumulate(3);
	hist.accumulate(4);
	hist.accumulate(4);
	hist.accumulate(4);
	hist.accumulate(4);
    hist.make_histogram();
	hist.print();
	cout << hist.percentile(0.5) << endl;
}
*/