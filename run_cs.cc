#include <cstdlib> 
#include "wavelets.h"

#include "matrix.tcc"

static const int NWSCALE = 7;	// wavelet scales

void testing() {
  Matrix<int> mona("mona.dat");
  Matrix< complex<float> > FFT = mona.fft(); 

  Matrix<bool> mask(FFT.nrows(), FFT.ncols());
  for (int i=0; i<mask.nrows(); ++i)
    for (int j=0; j<mask.ncols(); ++j) { 
      mask[i][j] = true;
      if ( rand() < RAND_MAX*0.9) mask[i][j] = false;
    }
  for (int i=0; i<10; ++i)
    for (int j=0; j<5; ++j) { 
      mask[i][j] = true;
  }

  FFT.apply_mask(mask);
  Matrix<float> image = FFT.ifft();
  image.save_map("x.map");

  FFT.fill(complex<float>(1, 0));

  FFT.apply_mask(mask);
  Matrix<float> psf = FFT.ifft();
 
  psf.save_map("psf.map");

  Matrix< complex<float> > z1(2, 2), z2(2, 2);
  z1.fill(complex<float>(1,2));
  z2[0][0] = complex<float>(2,1);
  z2[0][1] = complex<float>(5,2);
  z2[1][0] = complex<float>(1,1);
  z2[1][1] = complex<float>(2,6);
  Matrix< complex<float> > z3 = z2-z1;
  z3.print();

  Matrix<float> mona1("mona.dat");
  Wavelets wavelets(mona1);
  cout << wavelets.length_coefficients() <<endl;
  Matrix<float> mona2 = wavelets.reconstruct();
  mona2.save_map("w_recons.map");
}

void compare_fft(Matrix<complex<float> > FFT, Matrix<complex<float> > fmt) {
  for (int i=0; i<fmt.nrows(); ++i)
    for (int j=0; j<fmt.ncols(); ++j) if ( abs(fmt[i][j]) != 0 ) cout << fmt[i][j] << " " << FFT[i][j] << endl; 
}

int main() {
  float mu = 1;
  float threshold_lvl=0.005;
  int num_masked=0;

  // Works on input_data dumped from bifrost 
  Matrix<complex<float> > FFT("data.txt"); FFT.flip(); FFT.fftshift(); FFT.chop_negative(); 

  Matrix<bool> mask(FFT.nrows(), FFT.ncols());  // Create mask corresponding to missing FFT components
  for (int i=0; i<mask.nrows(); ++i)
    for (int j=0; j<mask.ncols(); ++j) { 
      if ( abs(FFT[i][j]) == 0 ) { mask[i][j] = false; ++num_masked; }
      else mask[i][j] = true;
    }
   mask.save_map("mask.map"); 
  
  FFT.apply_mask(mask);
  cout << "% masked: " << (float)num_masked/mask.nrows()/mask.ncols()*100 << endl; 
  // Now the FFT (masked) and mask are set, start compressed_sensing.

  // Get wavelet coefficients of dirty image
  Matrix<float> dirty_image = FFT.ifft();  dirty_image.fftshift();  
  dirty_image.save_map("dirty.map");

  Wavelets wavelets(dirty_image); 
  wavelets.stats();
 
  return 0;

  // Enter CS loop
  for (int i=0; i<200; ++i) {
    // Make an image of the current wavelet coefficients and make it positive
    Matrix<float> candidate_image = wavelets.reconstruct();      // wavelets -> image
    candidate_image.make_positive(); 

    // Get the difference between the image FFT (masked) and the original FFT components
    candidate_image.fftshift();
    Matrix< complex<float> > fft = candidate_image.fft(); 
    Matrix< complex<float> > fft_difference = FFT-fft;  
    fft_difference.apply_mask(mask); 

    double sum = 0.0;
    for (int i=0; i<fft_difference.nrows(); ++i)
      for (int j=0; j<fft_difference.ncols(); ++j) sum += norm(fft_difference.get(i, j)); 
    cout << "Diff: " << sqrt(sum/fft_difference.nrows()/fft_difference.ncols()) << endl;  

    // Turn the FFT difference into wavelet coeff difference
    Matrix<float> image_difference = fft_difference.ifft(); 
    image_difference.fftshift();   
    Wavelets diff_wavelets(image_difference);

    // Gradient step - adjust the current wavelet coeff by the difference
    wavelets.add_coefficients(diff_wavelets, mu);

    // Apply soft thresholding to the adjusted wavelet coeff which are a new image
    wavelets.soft_threshold(threshold_lvl);
    //wavelets.fast();	// Attempt fast convergence
    wavelets.stats();
    // Now have a new set of wavelet coeffs representing a new image - go around again
  }

  // Reconstruct final image and dump
  Matrix<float> image = wavelets.reconstruct();
  image.make_positive(); 
  image.save_map("x.map");

  // Get dirty image
  // Wavelet transform the image and threshold the coefficients wc

  // Loop ---
    // Convert wavelet coefficients->image	Image = Wavelet2Image(Wave_coeff)
    // Apply positivity to image		Image = MakePositive(Image)
    // Convert image->Fourier			FFT = DoFFT(Image)
    // Mask Fourier				Masked_FFT = Mask(FFT)
    // Get difference between Fourier and original Fourier		Difference_FFT = Masked_FFT-Samples_FFT
    // Convert difference to an image then to diff-wavelet coefficients		Difference_Image = inverse_FFT(Difference_FFT);   Difference_wave_coeff = Image2Wavelet(Difference_Image)
    // Apply the relaxation parameter (gain) to the diff-wavelet coefficients	
    // Add diff-wavelet coefficients onto the existing set		Current_wave_coeff += Diff_wave_coeff*mu
    // Apply thresholding to new wavelet coefficients		Current_wave_ceoff = Threshold(Current_wave_coeff)
  return 0;
}
         
/* Notes on Python format
Take input_data from IFFT2 block. After doing ifftshift, the UV plane shows
a clump around 0.
The U,V are symmetrical (conjugates) about point N/2-1 where N is the dimension.
So if N=256 the UV are symmetrical about 127 (not 128). However
they are not perfectly symmetrical. 

When numpy does an FFT2 it places the data in "corner" order i.e. everything
is shifted. This is the way that ifft wants it, and the way fftw3 wants it.
The centre is moved to the corners. The centre is at N/2. If this is the Python fft:

(53+0j) 5j (-1+0j) -5j
(-9-2j) (-2+1j) (-1+0j) (4+5j)
(1+0j) (6+3j) (-1+0j) (6-3j)
(-9+2j) (4-5j) (-1+0j) (-2-1j)

then this is the fftw3 fft:

(53,0) (0,5) (-1,0) 
(-9,-2) (-2,1) (-1,0) 
(1,0) (6,3) (-1,0) 
(-9,2) (4,-5) (-1,0) 

They are the same except for the negative frequncies being cut from the fftw.

np.fft.fftshift is the same as the ifftshift and it is it's own inverse.
there is only a difference between fftshift and ifftshift if N is odd.
*/
