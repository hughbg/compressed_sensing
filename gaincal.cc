#include <cstdlib>
#include <iostream>
#include "levmar-2.6/levmar.h"
#include "matrix.h"

using namespace std;

const int NUM_STANDS = 32;

JonesMatrix V[NUM_STANDS][NUM_STANDS];
JonesMatrix V_perturb[NUM_STANDS][NUM_STANDS];
JonesMatrix J[NUM_STANDS];
JonesMatrix J_perturb[NUM_STANDS];
JonesMatrix J_new[NUM_STANDS];
JonesMatrix P[NUM_STANDS][NUM_STANDS];



void perturb(JonesMatrix& j) {
  j[0][0] *= 1.0+((float)rand()/RAND_MAX-0.5)/2.5;
  j[0][1] *= 1.0+((float)rand()/RAND_MAX-0.5)/2.5;
  j[1][0] *= 1.0+((float)rand()/RAND_MAX-0.5)/2.5;
  j[1][1] *= 1.0+((float)rand()/RAND_MAX-0.5)/2.5;
}

float ratio(float x, float y) {
  if ( x == 0 && y == 0 ) return 0;
  else if ( x == 0 ) return fabs(y);
  else if ( y == 0 ) return fabs(x);
  else return fabs((x-y)/y);
}

float sqr(float x) { return x*x; }

complex<float> complex_rand() {
  float r = ((float)rand()/RAND_MAX-0.5)*10;
  float i = ((float)rand()/RAND_MAX-0.5)*10;

  return complex<float>(r, i);
}

double squared_norm(JonesMatrix& m) {   // Squared frobenius norm
  complex<float> a = m[0][0];
  complex<float> b = m[0][1];
  complex<float> c = m[1][0];
  complex<float> d = m[1][1];
  return abs(a*conj(a)+b*conj(b)+c*conj(c)+d*conj(d));  
}

float residual() {  // Between V and J.P.Jt
  double res = 0;
  for (int j=0; j<NUM_STANDS; ++j)
    for (int k=0; k<j; ++k) {    // Not using the whole thing
      JonesMatrix Vresidual = V_perturb[j][k]-(J[j]*P[j][k]*J[k].conjugate_transpose());
      res += squared_norm(Vresidual);
    }

  return sqrt(res);
}

void residual_for_opt(double *r) {  // Between V and J.P.Jt
  int index=0;
  for (int j=0; j<NUM_STANDS; ++j)
    for (int k=j+1; k<NUM_STANDS; ++k) {    
      JonesMatrix Vresidual = V_perturb[j][k]-(J[j]*P[j][k]*J[k].conjugate_transpose());
      r[index++] = squared_norm(Vresidual);
    }
}


void setup() {
  for (int j=0; j<NUM_STANDS; ++j) {
    J[j].set( complex_rand(), complex_rand(), complex_rand(), complex_rand() );
    for (int k=j+1; k<NUM_STANDS; ++k) {
      P[j][k].set( complex_rand(), complex_rand(), complex_rand(), complex_rand() );
      P[k][j] = P[j][k].conjugate_transpose();
    }
  }

  for (int j=0; j<NUM_STANDS; ++j) 
    for (int k=0; k<NUM_STANDS; ++k) {
      V[j][k] = J[j]*P[j][k]*J[k].conjugate_transpose();
    }

   
}

void method1() { 
  // Perturb J
  for (int j=0; j<NUM_STANDS; ++j) {
    J_perturb[j] = J[j];
    perturb(J_perturb[j]); 
  }

  // Calculate perturbed V
  for (int j=0; j<NUM_STANDS; ++j) 
    for (int k=0; k<NUM_STANDS; ++k) {
      V_perturb[j][k] = J_perturb[j]*P[j][k]*J_perturb[k].conjugate_transpose();
    }

  V_perturb[1][2].print();
  
  cout << "Residual " << residual() << endl;
  
  
  for (int attempts=0; attempts<500; ++attempts) {   // in range(36270):

    for (int j=0; j<NUM_STANDS; ++j) {
      JonesMatrix first_term;

      for (int k=0; k<NUM_STANDS; ++k) {	// problem with k=j+1 is that the top J is never computed. It always stays the same or 0
	if ( j != k ) {
          JonesMatrix multiplied = V_perturb[j][k]*J[k]*P[j][k].conjugate_transpose();
          first_term += multiplied;
	}
      }
       
      JonesMatrix second_term;
      for (int k=0; k<NUM_STANDS; ++k) {
	if ( j != k ) {
          JonesMatrix multiplied = P[j][k]*J[k].conjugate_transpose()*J[k]*P[j][k].conjugate_transpose();
          second_term += multiplied;
	}
      }

      J_new[j] = first_term*second_term.inverse();
      if ( !J_new[j].is_normal() ) { J_new[j] = J[j]; cerr << "Hit nan\n"; }    // nan

    }

    for (int j=0; j<NUM_STANDS; ++j) J[j] += (J_new[j]-J[j])*0.1;

  }
  
  cout << "Residual " << residual() << endl;
  
  JonesMatrix V_test = J[1]*P[1][2]*J[2].conjugate_transpose();
  V_test.print();
}

//double *p,         /* I/O: initial parameter estimates. On output contains the estimated solution */
//double *x,         /* I: measurement vector. NULL implies a zero vector */
//int m,             /* I: parameter vector dimension (i.e. #unknowns) */
//int n,             /* I: measurement vector dimension */
//void *adata)       /* I: pointer to possibly needed additional data, passed uninterpreted to func.
//                    * Set to NULL if not needed

static void errorfunc(double *p, double *x, int m, int n, void *adata) {
  for (int j=0; j<NUM_STANDS; ++j) {
    J[j][0][0] = complex<float>(p[j*8+0], p[j*8+1]);
    J[j][0][1] = complex<float>(p[j*8+2], p[j*8+3]);
    J[j][1][0] = complex<float>(p[j*8+4], p[j*8+5]);
    J[j][1][1] = complex<float>(p[j*8+6], p[j*8+7]);
  }
  residual_for_opt(x);
}

void method1_1() { 
  // Perturb J
  for (int j=0; j<NUM_STANDS; ++j) {
    J_perturb[j] = J[j];
    perturb(J_perturb[j]); 
  }
  
  
  // Calculate perturbed V
  for (int j=0; j<NUM_STANDS; ++j) 
    for (int k=0; k<NUM_STANDS; ++k) {
      V_perturb[j][k] = J_perturb[j]*P[j][k]*J_perturb[k].conjugate_transpose();
    }
  
  cout << "Residual " << residual() << endl;
  
  // Flatten 
  double *flat = new double[NUM_STANDS*8];
  for (int j=0; j<NUM_STANDS; ++j) {
    flat[j*8+0] = J[j][0][0].real(); flat[j*8+1] = J[j][0][0].imag();
    flat[j*8+2] = J[j][0][1].real(); flat[j*8+3] = J[j][0][1].imag();
    flat[j*8+4] = J[j][1][0].real(); flat[j*8+5] = J[j][1][0].imag();
    flat[j*8+6] = J[j][1][1].real(); flat[j*8+7] = J[j][1][1].imag();
   }
  int m = NUM_STANDS*8;
  int n = 32640;

  double *first = new double[n];
  residual_for_opt(first);

  dlevmar_dif(errorfunc, flat, NULL, m, n, 100, NULL, NULL, NULL, NULL, NULL);
  
  cout << "Residual " << residual() << endl;

  JonesMatrix V_test = J[1]*P[1][2]*J[2].conjugate_transpose();
  V_test.print();
}



int main() {
  setup();
  method1();
}

