
#include "matrix.h"

JonesMatrix JonesMatrix::inverse() {
  complex<float> a = data[0][0];
  complex<float> b = data[0][1];
  complex<float> c = data[1][0];
  complex<float> d = data[1][1];
  complex<float> det = a*d-b*c;

  JonesMatrix inverted(d/det, -b/det, -c/det, a/det);
  return inverted;
}

bool JonesMatrix::is_normal() {
  return isnormal(abs(data[0][0])) && isnormal(abs(data[0][1])) && isnormal(abs(data[1][0])) && isnormal(abs(data[1][1]));
}




